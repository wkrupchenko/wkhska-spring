package de.hska.wpf.spring.enterprise.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.hska.wpf.spring.enterprise.service.config.AppConfig;

@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:spring/applicationContext.xml"})
@ContextConfiguration(classes = {AppConfig.class})
public class HelloServiceIntegrationTest {
	
	@Autowired
	@Qualifier(value = "mockHelloService")
	private IHelloService helloService;
	
	@Autowired
	@Qualifier("helloServiceWithProps")
	private IHelloService helloServiceWithProps;
	
	@Autowired
	private IHelloService realHelloService;

	@Test
	public void helloWorldWithProps() {
		Assert.assertEquals("'Hello World' must be returned", "Hello World", helloServiceWithProps.getHello(null));
	}
	
	@Test
	public void helloWorldMock() {
		String result = helloService.getHello("World");
		Assert.assertNotNull("result must not be null", result);
		Assert.assertEquals("'Hello World' must be returned", "Hello World", result);
	}
	
	@Test
	public void helloWorldRealService() {
		Assert.assertEquals("'Hello World' must be returned", "Hello World", realHelloService.getHello("World"));
	}
	
}
