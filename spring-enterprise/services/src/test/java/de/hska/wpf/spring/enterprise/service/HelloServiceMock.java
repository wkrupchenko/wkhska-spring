package de.hska.wpf.spring.enterprise.service;

import org.springframework.stereotype.Service;

import de.hska.wpf.spring.enterprise.service.IHelloService;

@Service(value = "mockHelloService")
public class HelloServiceMock implements IHelloService {

	@Override
	public String getHello(String toGreet) {
		return "Hello " + toGreet;
	}

}
