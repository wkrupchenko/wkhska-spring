package de.hska.wpf.spring.enterprise.service;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service("helloServiceWithProps")
public class HelloServiceWithProps implements IHelloService {

	@Autowired
	private Environment environment;

	@Value("${hello}")
	private String hello;

	@Value("${world}")
	private String world;

	@Override
	public String getHello(String toGreet) {

		return hello + " " + world;
	}

	@PostConstruct
	private void initialize() {
		world = environment.getProperty("world");
		hello = environment.getProperty("hello");
	}

}
