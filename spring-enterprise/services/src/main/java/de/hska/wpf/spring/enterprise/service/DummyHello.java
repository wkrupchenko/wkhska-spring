package de.hska.wpf.spring.enterprise.service;

import org.springframework.stereotype.Component;

@Component
public class DummyHello {
	
	public String getHello() {
		return "Hello";
	}

}
